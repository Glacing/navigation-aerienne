
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 16:17:04 2020

@author: Ferre Aurélien / Roehrig Sylvain
"""

import math  
import numpy as np
from PIL import Image, ImageDraw
import tkinter as tki
from tkinter import messagebox

l = 1000


def calculOrthodromie(lata, latb, longa, longb) :
    A = math.sin(math.radians(lata))
    B = math.sin(math.radians(latb))
    C = math.cos(math.radians(lata))
    D = math.cos(math.radians(latb))
    E = math.cos(math.radians(abs(longa-longb)))

    F = (A*B)+(C*D*E)

    G = np.arccos(F)

    H = G * 6371
    return H

def choixDirection(aeo,beo,aeos, beos) :
    if aeos != beos :
        if beos == 1 :
            print("On part vers l'est")
            CHOIXDIRECTION = 1
        else :
           print("On part vers l'ouest")
           CHOIXDIRECTION = 2
    else :
        if abs(beo) > abs(aeo) :
            if beos == 1 :
                print("On part vers l'est")
                CHOIXDIRECTION = 1
            else :
                print("On part vers l'ouest")
                CHOIXDIRECTION = 2
        else:
            if beos == 1 :
                print("On part vers l'ouest")
                CHOIXDIRECTION = 2
            else :
                print("On part vers l'est")
                CHOIXDIRECTION = 1
    return CHOIXDIRECTION

def calculVecteur(lata, latb, longa, longb, CHOIXDIRECTION) :
    A = math.sin(math.radians(lata))
    B = math.sin(math.radians(latb))
    C = math.cos(math.radians(lata))
    D = math.cos(math.radians(latb))
    E = math.cos(math.radians(abs(longa-longb)))
    F = (A*B)+(C*D*E)

    G = np.arccos(F)
    
    V = (B - A * F)/(C*math.sin(G))
    if CHOIXDIRECTION == 1:
        ALPHA = np.arccos(V)
    else:
        ALPHA = 2*math.pi - np.arccos(V)
    return ALPHA

def latitudeNouveauPoint(lata, ALPHA) :
    latmp = math.radians(lata) + math.cos(ALPHA) * l * (math.pi / (1.852*60*180))
    return math.degrees(latmp)

def longitudeNouveauPoint(lata, longa, ALPHA):
    lonmp = math.radians(longa) + (math.sin(ALPHA)/math.cos(math.radians(lata))) * l * (math.pi / (1.852*60*180))
    return math.degrees(lonmp)

def tracerCroix(lata,longa,couleur,latprecedente,longprecedente, image):
    img=image
    draw = ImageDraw.Draw(img)
    x, y = img.size ##(2048,1024)
    
    
    
    a = x/360 ## a est le coefficient de conversion pixels/degres en longitude
    b = y/180
        
    # Le centre de l'image correspond a la jonction Equateur/meridien de Greenwich
        
    x=x/2
    y=y/2
        
    # En partant du centre de l'image (equivalent à (0,0)), on rajoute
    # la longitude et la latitude entres en parametres multiplies par
    # leurs coefficients de conversion respectifs (a et b)
    lata = -lata
    latprecedente = - latprecedente
    longitude = x + longa*a
    latitude = y + lata*b
    longitudeprecedente = x + longprecedente*a   
    latitudeprecedente = y + latprecedente*b
    
    if couleur == 1 :
        draw.line([(longitude-5,latitude-5),(longitude+5,latitude+5)], fill=(255,20,147), width=3)
        draw.line([(longitude-5,latitude+5),(longitude+5,latitude-5)], fill=(255,20,147), width=3)
    elif couleur == 2 :
        draw.line([(longitude-5,latitude-5),(longitude+5,latitude+5)], fill=(0,255,0), width=3)
        draw.line([(longitude-5,latitude+5),(longitude+5,latitude-5)], fill=(0,255,0), width=3)
    else :
        draw.line([(longitude-5,latitude-5),(longitude+5,latitude+5)], fill=250, width=3)
        draw.line([(longitude-5,latitude+5),(longitude+5,latitude-5)], fill=250, width=3)
    
    draw.line([(longitude,latitude),(longitudeprecedente,latitudeprecedente)], fill=250, width=1)
          
# Lorsque l'on appuie sur le bouton valider :
def boutonValider (y1,x1 , y2,x2):
    img = Image.open(r'Utm-zones.jpg')
    y1,x1,y2,x2=changerOrientation(y1, x1, y2, x2)
    print(x1)
    print(x2)
    print(y1)
    print(y2)
    tracerChemin(y1,x1 , y2,x2 , img)
    img.show()
    img.close()

# Selon les radioboutons activés, on change l'orientation
def changerOrientation(y1,x1,y2,x2):
    if varlat1.get() == 2:
        y1=-y1
    if varlong1.get() ==2:
        x1=-x1
    if varlat2.get() == 2:
        y2=-y2
    if varlong2.get() ==2:
        x2=-x2
    return y1,x1,y2,x2

def tracerChemin(y1,x1,y2,x2, img):
    
    a = y1
    b = y2
    aeo = x1
    beo = x2 
    
    if aeo > 180 or aeo < -180 :
        messagebox.showinfo("Conversion",
                            "Longitutde invalide :"+str(aeo)
                            +"\nConversion en longitutde valide...\n"
                            +"Nouvelle longitude : "+str(aeo%180))
        aeo=aeo%180
    if a > 90 or a < -90 :
        messagebox.showinfo("Conversion",
                            "Latitutde invalide :"+str(-a)
                            +"\nConversion en latitutde valide...\n"
                            +"Nouvelle latitude : "+str(-a%90))
        a=a%90
    
    if beo > 180 or beo < -180 :
        messagebox.showinfo("Conversion",
                            "Longitutde invalide :"+str(beo)
                            +"\nConversion en longitutde valide...\n"
                            +"Nouvelle longitude : "+str(beo%180))
        beo=beo%180
    if b > 90 or b < -90 :
        messagebox.showinfo("Conversion",
                            "Latitutde invalide :"+str(-b)
                            +"\nConversion en latitutde valide...\n"
                            +"Nouvelle latitude : "+str(-b%90))
        b=b%90

    if(aeo < 0):
        aeos = 2
    else:
        aeos = 1
    if(beo < 0):
        beos = 2
    else:
        beos = 1
        
    ortho = calculOrthodromie(a,b,aeo,beo)
    directionprogramme = choixDirection(aeo, beo, aeos, beos)
    compteur = 1
    lata = a
    latb = b
    longa = aeo
    longb = beo
    tracerCroix(lata,longa,1,lata,longa, img)
    if(longa != longb) :
        while compteur < ortho/l:
            ALPHA = calculVecteur(lata, latb, longa, longb, directionprogramme)
            print(ALPHA)
            latprecedente = lata
            longprecedente = longa
            lata = latitudeNouveauPoint(lata, ALPHA)
            longa = longitudeNouveauPoint(lata, longa, ALPHA)
            print(lata)
            print(longa)
            tracerCroix(lata,longa,0,latprecedente,longprecedente, img)
            compteur+=1
            print("----------------------")
            print(compteur)
    tracerCroix(latb,longb,2,lata,longa, img)

#Interface graphique
fenetre = tki.Tk()
fenetre['bg']='white'
fenetre.title(" M3202 - Navigation Aerienne par FERRE Aurelien et ROEHRIG Sylvain")
fenetre.minsize(800,480)



##Boutons de choix NESO (Nord Est Sud Ouest)

varlong1=tki.IntVar() #E/O premier bouton
varlat1 =tki.IntVar() #N/S premier bouton
varlong2=tki.IntVar() #E/O Second bouton
varlat2 =tki.IntVar() #N/S Second bouton


#Coordonnées du premier point
frame1 = tki.LabelFrame(fenetre, labelanchor=tki.N, text="Point de départ")
frame1.pack(expand=tki.TRUE, fill=tki.X, side=tki.LEFT)
subframe1_1 = tki.Frame(frame1)
subframe1_1.pack()
subframe1_2 = tki.Frame(frame1)
subframe1_2.pack()
tki.Radiobutton(subframe1_1, text='Est (par défaut)', value=1, variable=varlong1).pack(side=tki.RIGHT)
tki.Radiobutton(subframe1_1, text='Ouest', value=2, variable=varlong1).pack(side=tki.RIGHT)
tki.Radiobutton(subframe1_2, text='Nord (par défaut)', value=1, variable=varlat1).pack(side=tki.RIGHT)
tki.Radiobutton(subframe1_2, text='Sud', value=2, variable=varlat1).pack(side=tki.RIGHT)


#Coordonnées du second point
frame2 = tki.LabelFrame(fenetre,labelanchor=tki.N, text="Point de destination")
frame2.pack(expand=tki.TRUE,fill=tki.X, side=tki.RIGHT)
subframe2_1 = tki.Frame(frame2)
subframe2_1.pack()
subframe2_2 = tki.Frame(frame2)
subframe2_2.pack()
tki.Radiobutton(subframe2_1, text='Est (par défaut)', value=1, variable=varlong2).pack(side=tki.RIGHT)
tki.Radiobutton(subframe2_1, text='Ouest', value=2, variable=varlong2).pack(side=tki.RIGHT)
tki.Radiobutton(subframe2_2, text='Nord (par défaut)', value=1, variable=varlat2).pack(side=tki.RIGHT)
tki.Radiobutton(subframe2_2, text='Sud', value=2, variable=varlat2).pack(side=tki.RIGHT)






#Spinbox pour choisir les valeurs
minvar = tki.DoubleVar()
minvar.set(0)

##Premier point
#Longitude
tki.Label(subframe1_1, text="Longitude de depart :").pack()
long1 = tki.Spinbox(subframe1_1,from_=minvar.get(), to=179.99, format="%.2f")
long1.pack()
#Latitude
tki.Label(subframe1_2, text="Latitude de depart :").pack()
lat1 = tki.Spinbox(subframe1_2, from_=minvar.get(), to=89.99, format="%.2f")
lat1.pack()

##Second point
#Longitude
tki.Label(subframe2_1, text="Longitude d'arrivee :").pack()
long2 = tki.Spinbox(subframe2_1, from_=minvar.get(), to=179.99, format="%.2f")
long2.pack()
#Latitude
tki.Label(subframe2_2, text="Latitude d'arrivee :").pack()
lat2 = tki.Spinbox(subframe2_2, from_=minvar.get(), to=89.99, format="%.2f")
lat2.pack()


#Bouton valider
frame3 = tki.Frame(fenetre, bg='white', pady=100)
frame3.pack(side=tki.BOTTOM)
bouton=tki.Button(frame3, 
                  text="Valider", 
                  command=lambda : boutonValider(float(lat1.get()), float(long1.get()), float(lat2.get()), float(long2.get())),# Chercher pk on met lambda qd il y a des parametres  
                  padx=50, pady=10) 
bouton.pack(expand=tki.TRUE)

fenetre.mainloop()
